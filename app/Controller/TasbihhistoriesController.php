<?php
App::uses('AppController', 'Controller');
/**
 * Tasbihhistories Controller
 *
 * @property Tasbihhistory $Tasbihhistory
 * @property PaginatorComponent $Paginator
 */
class TasbihhistoriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Tasbihhistory->recursive = 0;
        $this->set('tasbihhistories', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->Tasbihhistory->exists($id)) {
            throw new NotFoundException(__('Invalid tasbihhistory'));
        }
        $options = array('conditions' => array('Tasbihhistory.' . $this->Tasbihhistory->primaryKey => $id));
        $this->set('tasbihhistory', $this->Tasbihhistory->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Tasbihhistory->create();
            if ($this->Tasbihhistory->save($this->request->data)) {
                $this->Flash->success(__('The tasbihhistory has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The tasbihhistory could not be saved. Please, try again.'));
            }
        }
        $users = $this->Tasbihhistory->User->find('list');
        $tasbihoptions = $this->Tasbihhistory->Tasbihoption->find('list');
        $this->set(compact('users', 'tasbihoptions'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        if (!$this->Tasbihhistory->exists($id)) {
            throw new NotFoundException(__('Invalid tasbihhistory'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Tasbihhistory->save($this->request->data)) {
                $this->Flash->success(__('The tasbihhistory has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The tasbihhistory could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Tasbihhistory.' . $this->Tasbihhistory->primaryKey => $id));
            $this->request->data = $this->Tasbihhistory->find('first', $options);
        }
        $users = $this->Tasbihhistory->User->find('list');
        $tasbihoptions = $this->Tasbihhistory->Tasbihoption->find('list');
        $this->set(compact('users', 'tasbihoptions'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->Tasbihhistory->id = $id;
        if (!$this->Tasbihhistory->exists()) {
            throw new NotFoundException(__('Invalid tasbihhistory'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Tasbihhistory->delete()) {
            $this->Flash->success(__('The tasbihhistory has been deleted.'));
        } else {
            $this->Flash->error(__('The tasbihhistory could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    public function add_tasbih_count(){
        $this->autoLayout = false;
        $this->autoRender = false;
        if($this->request->is('post')){

                $this->Tasbihhistory->create();
                if($this->Tasbihhistory->save($this->request->data)){
                    die(json_encode(['success' => true, 'msg' => 'Data has been saved']));
                }

            //die(pr($data));

        }
    }

    public function get_count_tasbih($user_id = null, $time = 'day')
    {
        $this->autoRender = false;
        $this->autoLayout = false;

        if ($time == 'day' || $time == 'week' || $time == 'month' || $time == 'year') {
            $tasbihData =  $this->_getData($user_id,$time);
            die(json_encode(['success' => true, 'data' => $tasbihData]));
        } else {
            $query = [
                'recursive' => -1,

                'fields' => [
                    'Tasbihoption.transliteration',
                    'Tasbihoption.id',
                ],
                'order' => 'Tasbihoption.id DESC'
            ];
            $tasbihoption_id = $this->Tasbihhistory->Tasbihoption->find('all', $query);
            $overall = [];
            $finaldate = [];
            foreach($tasbihoption_id as $tasbih_id){
                $overall = $this->_get_count_tasbih_overall($tasbih_id['Tasbihoption']['id'], $user_id, $tasbih_id['Tasbihoption']['transliteration']);

                array_push($finaldate, $overall);
            }
            //pr($finaldate);die;
            $finaldate = Hash::sort($finaldate, '{n}.count.overall', 'desc','regular');
            die(json_encode(['success' =>true, 'data' =>$finaldate]));
        }
        //pr($tasbihoption_id);
    }
    private function _getData($user_id = null, $time = null){
        if ($time == 'day') {
            $sdate = date("Y-m-d");
            $edate = date("y-m-d");
        } elseif ($time == 'week') {
            $sdate = date('y-m-d', strtotime("last monday"));
            $edate = date("y-m-d");
        } elseif ($time == 'month') {
            $sdate = date('y-m-01');
            $edate = date("y-m-d");
        } elseif ($time == 'year') {
            $sdate = date('y-01-01');
            $edate = date("y-m-d");
        }
        $query = [
            'conditions' => [
                'date(Tasbihhistory.created) BETWEEN ? AND ?' => array($sdate,$edate),
                'user_id' => $user_id
            ],
            'recursive' => 0,
            'fields' => [
                'SUM(Tasbihhistory.count) as count',
                'Tasbihoption.transliteration',
                'Tasbihoption.id',
            ],
            'group' => 'Tasbihhistory.tasbihoption_id',
            'order' => 'SUM(Tasbihhistory.count) DESC'
        ];
        $data = $this->Tasbihhistory->find('all', $query);
        $used_option_ids = Hash::extract($data, '{n}.Tasbihoption.id');
        //pr($data);die;
        $conditions = [];
        if(!empty($used_option_ids)) {
            $conditions = am($conditions,[
                'Tasbihoption.id NOT IN' => $used_option_ids
            ]);
        }
        $query_1 = [
            'recursive' =>-1,
            'fields' => [
                'Tasbihoption.transliteration',
                'Tasbihoption.id',
            ],
            'conditions' => $conditions
        ];
        $data_1 = $this->Tasbihhistory->Tasbihoption->find('all',$query_1);
        $res = am($data, $data_1);
        if(!empty($res)){
            foreach($res as $d){
                $gData[] = [
                    'transliteration_id' => $d['Tasbihoption']['id'],
                    'transliteration' => $d['Tasbihoption']['transliteration'],
                    'count' => !empty($d['0']['count']) ? intval($d['0']['count']) : 0,

                ];
            }
        } else {
            $gData = array();
        }

        return $gData;
    }
    private function _get_count_tasbih_overall($tasbihoption_id = null,$user_id = null, $name = null){
        $querytoday = [
            'conditions' =>[
                'date(Tasbihhistory.created) BETWEEN ? AND ?' => array(date("Y-m-d"),date("y-m-d")),
                'user_id' => $user_id,
                'tasbihoption_id' => $tasbihoption_id
            ],
            'recursive' => -1,
            'fields' => [
                'SUM(Tasbihhistory.count) as count'
            ],
            'group' => 'Tasbihhistory.tasbihoption_id'

        ];
        $today = $this->Tasbihhistory->find('first', $querytoday);
        //pr ($today);die;
        $queryweek = [
            'conditions' =>[
                'date(Tasbihhistory.created) BETWEEN ? AND ?' => array(date('y-m-d', strtotime("last saturday")),date("y-m-d")),
                'user_id' => $user_id,
                'tasbihoption_id' => $tasbihoption_id
            ],
            'recursive' => -1,
            'fields' => [
                'SUM(Tasbihhistory.count) as count'
            ],
            'group' => 'Tasbihhistory.tasbihoption_id'

        ];
        $week = $this->Tasbihhistory->find('first', $queryweek);
        //pr ($week);
        $querymonth = [
            'conditions' =>[
                'date(Tasbihhistory.created) BETWEEN ? AND ?' => array(date('y-m-01'),date("y-m-d")),
                'user_id' => $user_id,
                'tasbihoption_id' => $tasbihoption_id
            ],
            'recursive' => -1,
            'fields' => [
                'SUM(Tasbihhistory.count) as count'
            ],
            'group' => 'Tasbihhistory.tasbihoption_id'

        ];
        $month = $this->Tasbihhistory->find('first', $querymonth);
        //pr ($month);
        $queryyear = [
            'conditions' =>[
                'date(Tasbihhistory.created) BETWEEN ? AND ?' => array(date('y-01-01'),date("y-m-d")),
                'user_id' => $user_id,
                'tasbihoption_id' => $tasbihoption_id
            ],
            'recursive' => -1,
            'fields' => [
                'SUM(Tasbihhistory.count) as count'
            ],
            'group' => 'Tasbihhistory.tasbihoption_id'

        ];
        $year = $this->Tasbihhistory->find('first', $queryyear);
        //pr ($year);die;
        $queryall = [
            'conditions' =>[
                'user_id' => $user_id,
                'tasbihoption_id' => $tasbihoption_id
            ],
            'recursive' => -1,
            'fields' => [
                'SUM(Tasbihhistory.count) as count'
            ],
            'group' => 'Tasbihhistory.tasbihoption_id'

        ];
        $all = $this->Tasbihhistory->find('first', $queryall);
        //pr ($all);die;
        $data = [
            'transliteration_id' =>$tasbihoption_id,
            'transliteration' =>$name,
            'count' =>[
                'today' =>!empty($today['0']['count']) ? intval($today['0']['count']) : 0,
                'week' => !empty($week['0']['count']) ? intval($week['0']['count']) : 0,
                'month' => !empty($month['0']['count']) ? intval($month['0']['count']) : 0,
                'year' => !empty($year['0']['count']) ? intval($year['0']['count']) : 0,
                'overall' => !empty($all['0']['count']) ? intval($all['0']['count']) : 0,
            ]
        ];
        return $data;




    }
    public function edit_tasbih_histories($user_id = null , $time = 'day'){
        if ($time == 'day') {
            $sdate = date("Y-m-d");
            $edate = date("y-m-d");
        } elseif ($time == 'week') {
            $sdate = date('y-m-d', strtotime("last monday"));
            $edate = date("y-m-d");
        } elseif ($time == 'month') {
            $sdate = date('y-m-01');
            $edate = date("y-m-d");
        } elseif ($time == 'year') {
            $sdate = date('y-01-01');
            $edate = date("y-m-d");
        }

        $query = [
            'conditions' =>[
                'user_id' => $user_id,
            ],
            'recursive' => 0,
            'fields' => ['Tasbihhistory.tasbihoption_id','Tasbihoption.transliteration'],
            'group' => 'Tasbihhistory.tasbihoption_id'

        ];
        $data = $this->Tasbihhistory->find('list', $query);
        $finalData = [];
        foreach($data as $key=>$d){
            $result = $this->_getEditData($key,$d,$sdate,$edate);
            if(!empty($result)) {
                $finalData[] = $result;
            }
        }
        die(json_encode(['success' => true,'data' => $finalData]));
        //pr($finalData);die;
    }
    private function _getEditData($option_id = null,$name = null, $sdate = null,$edate = null){
        $conditions = [];
        if(!empty($sdate)&&!empty($edate)){
            $conditions = [$conditions,['date(Tasbihhistory.created) BETWEEN ? AND ?' => array($sdate,$edate)]];
        }
        $query_1 = [
            'conditions' =>am($conditions,[
                'Tasbihhistory.tasbihoption_id'=>$option_id,

            ]),
            'recursive' => -1,
            'fields' => [
                'Tasbihhistory.id',
                'Tasbihhistory.count',
                'Tasbihhistory.created'
            ]

        ];
        $data2 = $this->Tasbihhistory->find('all', $query_1);
        $call = ['option_name' => $name];
        foreach($data2 as $d){
            $data['data'][] = [
                'id' => $d['Tasbihhistory']['id'],
                'count' => intval($d['Tasbihhistory']['count']),
                'created' => $d['Tasbihhistory']['created']
            ];
        }
        if(empty($data)){
            return [];
        } else {
            return am($call,$data);
        }

    }
    public function delete_tasbih(){
        ;
        foreach($this->request->data['Tasbihhistory']['id']as $id){
            $this->admin_delete($id);
        }
        die(json_encode(['success' => true]));
    }
}
