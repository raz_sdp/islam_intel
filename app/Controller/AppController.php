<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Session', 'RequestHandler','Cookie',
        'Auth' => array(
            'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard', 'admin' => true),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'authError' => 'You are not allowed',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email', 'password' => 'password')
                )
            )
        )
    );
    public function beforeFilter(){
        $this->Auth->allow(
            'login','signup','get_tasbih_list','add_tasbih_count','get_count_tasbih','reset_password','validate_input',
            'test','get_help','forget_password','adminforgotpassword','getdata','edit_tasbih_histories','delete_tasbih'
        );


    }

    function _setErrorLayout() {
        if ($this->name == 'CakeError') {
            $this->layout = 'login';
        }
    }
    function beforeRender () {
        $this->_setErrorLayout();
    }
    public function _upload($file, $folder = null){
        //App::import('Vendor', 'phpthumb', array('file' => 'phpthumb' . DS . 'phpthumb.class.php'));

        if(is_uploaded_file($file['tmp_name'])){

            $ext  = strtolower(array_pop(explode('.',$file['name'])));

            if($ext == 'txt') $ext = 'jpg';

            $fileName = time() . rand(1,999) . '.' .$ext;

            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'pdf' || $ext == 'doc' || $ext == 'docx'){

                $uplodFile = WWW_ROOT.'files'.DS. $folder . DS .$fileName;

                if(move_uploaded_file($file['tmp_name'],$uplodFile)){

                    /*$dest_small = WWW_ROOT . 'files' . DS . $folder . DS . 'small' . DS . $fileName;

                    $this->_resize($uplodFile, $dest_small);*/

                    //$this->_orientation($uplodFile, $uplodFile);

                    //@unlink($uplodFile);

                    return $fileName;

                }

            }

        }

    }
}
