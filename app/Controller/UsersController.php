<?php
App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController
{

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');


    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function signup()
    {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $exitEmail = $this->User->findByEmail($this->request->data['User']['email']);
            if (!empty($exitEmail)) {
                die(json_encode(['success' => false, 'msg' => 'This email already exists']));
            }
            $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->User->recursive = -1;
                $is_user = $this->User->findById($this->User->id);
                unset($is_user['User']['password']);
                die(json_encode(['success' => true, 'msg' => 'You have registered successfully', 'userdata' => $is_user['User']]));
            } else {
                die(json_encode(['success' => false, 'msg' => 'could not be saved']));
            }
        }
    }

    public function login()
    {
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $email = $this->request->data['User']['email'];
            $password = AuthComponent::password($this->request->data['User']['password']);
            $this->User->recursive = -1;
            $is_user = $this->User->findByEmailAndPassword($email, $password);

            //$is_user['User']['test_id'] = $test_id;

            if (!empty($is_user)) {
                unset($is_user['User']['password']);
                die(json_encode(array('success' => true, 'userdata' => $is_user['User'])));
            } else {
                die(json_encode(array('success' => false, 'msg' => "incorrect password"))); //Or click 'Forgot password?' to reset it now
            }
        } else {
            die(json_encode(array('success' => false, 'msg' => 'Sorry!Inavalid request')));
        }
    }

    public function reset_password()
    {
        $to = $this->request->data['User']['email'];
        $is_user = $this->User->findByEmail($to);
        if (!empty($is_user)) {
            $new_passkey = $this->_generate_password(8);
            //$hash_passkey = AuthComponent::password($new_passkey);
            $this->User->id = $is_user['User']['id'];
            if ($this->User->saveField('reset_key', $new_passkey)) {
                try {
                    App::uses('CakeEmail', 'Network/Email');
                    $email = new CakeEmail();
                    $email->config('smtp');
                    $email->subject("Islam Intel - Password reset!");
                    $email->to($to);
                    $url = Router::url('/reset_password/'.$new_passkey, true);
                    //die($url);
                    $body = '<!DOCTYPE html>
<html>
<head>
</head>

<body style="font-family: Arial; font-size: 12px;">
<div>
Hi ' . $is_user['User']['fullname'] .
                        '<p>
                            You have requested a password reset, please follow the link below to reset your password.
                        </p>
                        <p>
                            <a href="'.$url.'">
                                Follow this link to reset your password.
                            </a>
                        </p>
                        <p>
                            Please ignore this email if you did not request a password change.
                        </p>
                        <br/>Regards,<br/><br/>The Islamic App Team
                    </div>
                    </body>
                    </html>';
                    $email->send($body);

                    /*$this->_send_email(
                        $email,
                        "Welcome to Islamic App!",
                        "Dear " . $is_user['User']['fullname'] . ",<br/>Your account  password has been reset. Your new password is <b>".$new_passkey."<br/><br/>Regards,<br/><br/>The Islamic App Team",
                        'welcome',
                        array('title' => 'Reseted your password')
                    );*/
                } catch (Exception $e) {
                    //echo($e);
                    die(json_encode(array('success' => false, 'msg' => 'Incorrect email address')));
                }
                die(json_encode(array('success' => true, 'msg' => 'Password reset link sent to your email')));
            } else {
                die(json_encode(array('success' => false, 'msg' => 'Sorry! something went wrong')));
            }


        } else {
            die(json_encode(array('success' => false, 'msg' => 'This mail is not registered.')));
        }
    }

    private function _generate_password($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];

        $password = str_shuffle($password);

        if (!$add_dashes)
            return $password;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function forget_password()
    {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            $resetkey = $this->params['reset_key'];
            if(!empty($resetkey)) {
                $user = $this->User->find('first', ['recursive'=>-1,'conditions' => ['User.reset_key' =>$resetkey]]);
                if(!empty($user['User']) && !empty($resetkey)) {
                    $new_password = $this->request->data['User']['password'];
                    $data = array(
                        'User' => array(
                            'password' => AuthComponent::password($new_password),
                            'reset_key' => null
                        )
                    );
                    $this->User->id = $user['User']['id'];
                    if ($this->User->save($data, false)) {
                        $this->Session->setFlash(__('Password has been reset successfully'), 'default', array('class' => 'alert alert-success text-center'));
                        //$this->redirect(Router::url( $this->referer(), true ));
                    }
                } else {
                    $this->Session->setFlash(__('Invalid Request'), 'default', array('class' => 'alert alert-danger text-center'));
                    //$this->redirect(Router::url( $this->referer(), true ));
                }
            }else {
                $this->Session->setFlash(__('Invalid Request'), 'default', array('class' => 'alert alert-danger text-center'));
                //$this->redirect(Router::url( $this->referer(), true ));
            }
        }
    }

    public function validate_input()
    {
        $this->autoRender = false;
        $this->autoLayout = false;
        //echo $app; die;
        if ($this->request->is('post')) {
            $key = key($this->request->data['User']);
            $input = $this->request->data['User'][$key];

            if ($key == 'email') {
                if (!filter_var($input, FILTER_VALIDATE_EMAIL)) {
                    die(json_encode(['success' => false, 'msg' => 'invalid (e.g. joe.bloggs@gmail.com)']));
                }
            }


            $this->User->recursive = -1;
            $is_exist = $this->User->find('first', [
                'conditions' => ['User.' . $key => $input]
            ]);
            if (!empty($is_exist)) {
                die(json_encode(['success' => false, 'msg' => 'This email is already exist.']));
            } else {
                die(json_encode(['success' => true]));
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null)
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->User->delete()) {
            $this->Session->setFlash(__('The user has been deleted'), 'default', array('class' => 'alert alert-success text-center'));
        } else {
            $this->Session->setFlash(__('The user could not be deleted. Please, try again'), 'default', array('class' => 'alert alert-warning text-center'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_login()
    {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['password']);
            //$password = $this->request->data['User']['password'];
            #die($password);
            #print_r($this->request->data);die;
            $query = array(
                'conditions' => array(
                    'User.email' => $this->request->data['User']['email'],
                    'User.password' => $password,

                    'User.role' => 'admin'

                )
            );
            //print_r($query);die;
            $this->User->recursive = -1;
            $is_exist = $this->User->find('first', $query);
            if (!empty($is_exist)) {
                /* $user_data = am(
                     $this->request->data['User'],
                     ['id' => $is_exist['User']['id']],
                     ['role' => 'admin']
                 );*/
                $this->Auth->login($is_exist['User']);
                $this->redirect(array('action' => 'dashboard', 'admin' => true));
            } else {
                $this->Auth->logout();
                $this->Session->setFlash(__('Incorrect username or password, please try again.'), 'default', array('class' => 'alert alert-danger text-center'));
            }
        }


    }
//    public function test($test = []){
//$this->autoRender=false;
//        $postdata = file_get_contents("php://input");
////        {'fullname':'mukul'}
////        echo ('dfgnm,kjjk,hmgfdc');
////        $this->User->create();
////            $this->User->save($this->request->data);
//       $data = json_decode($postdata, true);
//        $this->User->create();
//        $this->User->saveField('fullname', $data['fullname']);
//
////        data[test][data]
//    }
    public function admin_dashboard()
    {
        $users = $this->User->find('count');
        $this->set(compact('users'));
    }

    public function admin_index()
    {
        $options = [
            'conditions' => [
                'User.role' => null
            ],
            'order' => 'User.id ASC'
        ];
        $this->Paginator->settings = $options;
        $this->User->recursive = -1;
        $this->set('users', $this->Paginator->paginate('User'));
    }

    public function admin_logout()
    {
        if (AuthComponent::user()['role'] == 'admin') {
            $this->Auth->logout();
            $this->redirect('/admin');
        } else {
            $this->Auth->logout();
            $this->redirect('/');
        }
    }

    public function admin_settings()
    {
        //pr(AuthComponent::user());
        if ($this->request->is('post')) {
            $password = AuthComponent::password($this->request->data['User']['password']);
            $this->User->id = AuthComponent::user()['id'];
            if ($this->User->saveField('password', $password)) {
                $this->Session->setFlash(__('Your password changed.'), 'default', array('class' => 'alert alert-success text-center'));
            } else {
                $this->Session->setFlash(__('Something went wrong. please try again.'), 'default', array('class' => 'alert alert-success text-center'));
            }
        }
    }
    public function adminforgotpassword(){
        //$this->autoLayout = false;
        $this->layout = 'login';
        $this->render();
    }

}
