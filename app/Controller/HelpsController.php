<?php
App::uses('AppController', 'Controller');
/**
 * Helps Controller
 *
 * @property Help $Help
 * @property PaginatorComponent $Paginator
 */
class HelpsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Help->recursive = 0;
		$this->set('helps', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Help->exists($id)) {
			throw new NotFoundException(__('Invalid help'));
		}
		$options = array('conditions' => array('Help.' . $this->Help->primaryKey => $id));
		$this->set('help', $this->Help->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Help->create();
			if ($this->Help->save($this->request->data)) {
				$this->Flash->success(__('The help has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The help could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Help->exists($id)) {
			throw new NotFoundException(__('Invalid help'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Help->save($this->request->data)) {
                $this->Session->setFlash(__('The help has been saved.'), 'default', array('class' => 'alert alert-success text-center'));
				return $this->redirect(array('action' => 'index'));
			} else {
                $this->Session->setFlash(__('The help could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
			}
		} else {
			$options = array('conditions' => array('Help.' . $this->Help->primaryKey => $id));
			$this->request->data = $this->Help->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Help->id = $id;
		if (!$this->Help->exists()) {
			throw new NotFoundException(__('Invalid help'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Help->delete()) {
			$this->Flash->success(__('The help has been deleted.'), 'default', array('class' => 'alert alert-success text-center'));
		} else {
			$this->Flash->error(__('The help could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    public function get_help(){
        $options = [
            'recursive' => -1,
            'fields' =>[
                'Help.id',
                'Help.module_name',
                'Help.page_title',
                'Help.description'
            ]
        ];
        $help = $this->Help->find('all',$options);
        $result = [];
        $res = [];
        foreach($help as $h) {
            $res['module_name'] = trim($h['Help']['module_name']);
            $res['page_title'] = trim($h['Help']['page_title']);
            $data = preg_split('/([0-9]+\.)/', trim($h['Help']['description']));
            if(count($data)>1) {
                unset($data[0]);
                $description = array();
                foreach($data as $new) {
                    array_push($description, trim($new));
                }
                $res['description'] = $description;
            } else {
                $res['description'] = $data;
            }
            array_push($result, $res);
        }
        die(json_encode(['data' =>$result]));
    }
    public function admin_add_file()
    {


        if ($this->request->is('post')) {

             //pr($this->request->data);die;

            $filename = $this->request->data['User']['file']['tmp_name'];

            $mimes = array('application/vnd.ms-excel','text/xls','text/csv','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if ($this->request->data['User']['file']['size'] > 0 && in_array($this->request->data['User']['file']['type'], $mimes)) {

                $file = fopen($filename, "r");

                $count = 0;

                $this->Help->query('TRUNCATE TABLE helps');

                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {

                    if ($count > 0 && !empty($getData[1])) {
                        $data['Help'] = [

                            'module_name' => $getData['0'],

                            'page_title' => $getData['1'],

                            'description' => $getData['2'],

                        ];


                        $this->Help->create();

                        if ($this->Help->save($data)) {

                            $this->Session->setFlash(__('The data inserted successfully.'), 'default', array('class' => 'alert alert-success text-center'));

                        } else {

                            $this->Session->setFlash(__('Something went wrong. Please, try again.'), 'default', array('class' => 'alert alert-danger text-center'));

                        }

                    }

                    $count++;

                }


                fclose($file);


            } else {

                $this->Session->setFlash(__('It\'s not a vaild format'), 'default', array('class' => 'alert alert-danger text-center'));

            }


        }


    }
}
