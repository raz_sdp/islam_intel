<?php
App::uses('AppModel', 'Model');
/**
 * Tasbihhistory Model
 *
 * @property User $User
 * @property Tasbihoption $Tasbihoption
 */
class Tasbihhistory extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Tasbihoption' => array(
			'className' => 'Tasbihoption',
			'foreignKey' => 'tasbihoption_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
