<div class="tasbihoptions form">
<?php echo $this->Form->create('Tasbihoption'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Tasbihoption'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('transliteration');
		echo $this->Form->input('meaning');
		echo $this->Form->input('explanation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tasbihoptions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Tasbihhistories'), array('controller' => 'tasbihhistories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tasbihhistory'), array('controller' => 'tasbihhistories', 'action' => 'add')); ?> </li>
	</ul>
</div>
