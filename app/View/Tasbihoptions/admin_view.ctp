<div class="tasbihoptions view">
<h2><?php echo __('Tasbihoption'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tasbihoption['Tasbihoption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($tasbihoption['Tasbihoption']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Transliteration'); ?></dt>
		<dd>
			<?php echo h($tasbihoption['Tasbihoption']['transliteration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Meaning'); ?></dt>
		<dd>
			<?php echo h($tasbihoption['Tasbihoption']['meaning']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Explanation'); ?></dt>
		<dd>
			<?php echo h($tasbihoption['Tasbihoption']['explanation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($tasbihoption['Tasbihoption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($tasbihoption['Tasbihoption']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tasbihoption'), array('action' => 'edit', $tasbihoption['Tasbihoption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tasbihoption'), array('action' => 'delete', $tasbihoption['Tasbihoption']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $tasbihoption['Tasbihoption']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Tasbihoptions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tasbihoption'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tasbihhistories'), array('controller' => 'tasbihhistories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tasbihhistory'), array('controller' => 'tasbihhistories', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Tasbihhistories'); ?></h3>
	<?php if (!empty($tasbihoption['Tasbihhistory'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Tasbihoption Id'); ?></th>
		<th><?php echo __('Count'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tasbihoption['Tasbihhistory'] as $tasbihhistory): ?>
		<tr>
			<td><?php echo $tasbihhistory['id']; ?></td>
			<td><?php echo $tasbihhistory['user_id']; ?></td>
			<td><?php echo $tasbihhistory['tasbihoption_id']; ?></td>
			<td><?php echo $tasbihhistory['count']; ?></td>
			<td><?php echo $tasbihhistory['created']; ?></td>
			<td><?php echo $tasbihhistory['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tasbihhistories', 'action' => 'view', $tasbihhistory['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tasbihhistories', 'action' => 'edit', $tasbihhistory['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tasbihhistories', 'action' => 'delete', $tasbihhistory['id']), array('confirm' => __('Are you sure you want to delete # %s?', $tasbihhistory['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tasbihhistory'), array('controller' => 'tasbihhistories', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
