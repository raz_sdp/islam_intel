<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Admin Edit Help</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo $this->Form->create('Tasbihoption', ['type' => 'file']); ?>
                        <?php echo $this->Form->input('id', ['class' => 'form-control']); ?>
                        <div class="form-group">
                            <?php echo $this->Form->input('name', ['class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('transliteration', ['class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('meaning', ['class' => 'form-control']); ?>
                        </div>

                        <div class="form-group">
                            <label for="comment">Description:</label>
                            <textarea class="form-control" name="data[Tasbihoption][explanation]" rows="5" id="comment"><?php echo $this->data['Tasbihoption']['explanation'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="upload">Shared Image</label>
                            <input type="file" name="data[Tasbihoption][image]" class="form-control">
                        </div>
                        <div class="form-group">
                            <?php
                            if(!empty( $this->data['Tasbihoption']['image'])){
                                ?>
                                <img src="<?php echo $this->html->url('/files/share_images/'.$this->data['Tasbihoption']['image'], true); ?>" width="200">
                            <?php
                            }
                            ?>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-block btn-primary btn-lg">Update</button>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>



