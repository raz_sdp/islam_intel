<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tashbih options</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">

                            <?php echo $this->Form->create('Tasbihoption', array('type' => 'get', 'url' => array('page' => '1'), 'class' => 'form-horizontal')); ?>

                            <div class="col-md-3 pull-right">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder=""
                                           value="<?php echo @$keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            </form>
                            <!-- /.col -->
                        </div>

                        </br>
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id', '#'); ?></th>
                                <th><?php echo $this->Paginator->sort('name'); ?></th>
                                <th><?php echo $this->Paginator->sort('transliteration'); ?></th>
                                <th><?php echo $this->Paginator->sort('meaning'); ?></th>
                                <th><?php echo $this->Paginator->sort('explanation'); ?></th>
                                <th><?php echo $this->Paginator->sort('image'); ?></th>

                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tasbihoptions as $tasbihoption): ?>
                                <tr>
                                    <td><?php echo h($tasbihoption['Tasbihoption']['id']); ?>&nbsp;</td>
                                    <td><?php echo h($tasbihoption['Tasbihoption']['name']); ?>&nbsp;</td>
                                    <td><?php echo h($tasbihoption['Tasbihoption']['transliteration']); ?>&nbsp;</td>
                                    <td><?php echo h($tasbihoption['Tasbihoption']['meaning']); ?>&nbsp;</td>
                                    <td><?php echo h($tasbihoption['Tasbihoption']['explanation']); ?>&nbsp;</td>
                                    <td>
                                        <?php
                                        if(!empty($tasbihoption['Tasbihoption']['image'])){
                                            ?>
                                            <img src="<?php echo $this->html->url('/files/share_images/'.$tasbihoption['Tasbihoption']['image'], true); ?>" width="60">
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td class="actions">
                                        <!--			--><?php //echo $this->Html->link(__('View'), array('action' => 'view', $help['Help']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tasbihoption['Tasbihoption']['id']), ['target' => '_blank', 'class' => 'btn btn-warning']); ?>
                                        <!--			--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $help['Help']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $help['Help']['id']))); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>

                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
                            echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
                            echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
                            ?>
                        </ul>


                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>