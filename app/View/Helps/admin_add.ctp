<div class="helps form">
<?php echo $this->Form->create('Help'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Help'); ?></legend>
	<?php
		echo $this->Form->input('module_name');
		echo $this->Form->input('page_title');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Helps'), array('action' => 'index')); ?></li>
	</ul>
</div>
