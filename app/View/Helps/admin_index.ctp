<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Help Text</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
	<table id="example2" class="table table-bordered table-hover">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id','#'); ?></th>
			<th><?php echo $this->Paginator->sort('module_name'); ?></th>
			<th><?php echo $this->Paginator->sort('page_title'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($helps as $help): ?>
	<tr>
		<td><?php echo h($help['Help']['id']); ?>&nbsp;</td>
		<td><?php echo h($help['Help']['module_name']); ?>&nbsp;</td>
		<td><?php echo h($help['Help']['page_title']); ?>&nbsp;</td>
		<td><?php echo h($help['Help']['description']); ?>&nbsp;</td>
		<td><?php echo h($help['Help']['created']); ?>&nbsp;</td>
		<td><?php echo h($help['Help']['modified']); ?>&nbsp;</td>
		<td class="actions">
<!--			--><?php //echo $this->Html->link(__('View'), array('action' => 'view', $help['Help']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $help['Help']['id']), ['target' => '_blank', 'class' => 'btn btn-warning']); ?>
<!--			--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $help['Help']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $help['Help']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                        </ul>



                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
