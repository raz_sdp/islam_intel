
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upload help file</h3>
                    </div>

                    <div class="box-body">

                        <div class="box-body">
<form method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="upload">Select to upload csv file</label>
        <input type="file" name="data[User][file]" class="form-control" id="upload">
    </div>
    <div class="col-md-12">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <button class="btn btn-block btn-primary btn-lg" type="submit">Submit</button>
    </div>
        <div class="col-md-3"></div>
    </div>
</form>






                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>