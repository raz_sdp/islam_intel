<div class="helps view">
<h2><?php echo __('Help'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($help['Help']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Module Name'); ?></dt>
		<dd>
			<?php echo h($help['Help']['module_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Page Title'); ?></dt>
		<dd>
			<?php echo h($help['Help']['page_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($help['Help']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($help['Help']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($help['Help']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Help'), array('action' => 'edit', $help['Help']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Help'), array('action' => 'delete', $help['Help']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $help['Help']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Helps'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Help'), array('action' => 'add')); ?> </li>
	</ul>
</div>
