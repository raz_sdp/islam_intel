<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Admin Edit Help</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo $this->Form->create('Help'); ?>
                        <div class="form-group hide">
                            <?php echo $this->Form->input('id', ['class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('module_name', ['class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('page_title', ['class' => 'form-control']); ?>
                        </div>
<!--                        <div class="form-group">-->
<!--                            --><?php ////echo $this->Form->input('question', ['id' => 'editor', 'escape' => false]);?>
<!--                            <label>Description</label>-->
<!--                            <textarea cols="80" id="editor" name="data[Help][description]"-->
<!--                                      rows="10">--><?php //echo $this->data['Help']['description'] ?><!--</textarea>-->
<!--                        </div>-->

                        <div class="form-group">
                            <label for="comment">Description:</label>
                            <textarea class="form-control" name="data[Help][description]" rows="5" id="comment"><?php echo $this->data['Help']['description'] ?></textarea>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-block btn-primary btn-lg">Update</button>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
