<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->

<section class="sidebar">

<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <?php echo $this->Html->image('/img/avatar/default.png', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
    </div>
    <div class="pull-left info">
        <p> <?php echo(AuthComponent::user(['fullname'])); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>
<!-- search form -->

<!-- /.search form -->
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
<li class="header">MAIN NAVIGATION</li>

<!-- Manage Dashboard-->

<!--/Manage Dashboard-->
<?php
/*echo '<pre>';
print_r(AuthComponent::user()['role']);die;*/
$ctrl = strtolower($this->params['controller']);
$action = strtolower($this->params['action']);
$active = @($this->params[pass][0]);
?>

<!--Manage Users-->
<li class="<?php echo $ctrl == 'users' && $action == 'admin_dashboard' ? 'active' : '' ?>">
    <a href="<?php echo $this->Html->url("/dashboard", true) ?>">
        <i class="fa fa-dashboard"></i> <span class="">Dashboard</span>
    </a>
</li>

        <li class="<?php echo $ctrl == 'users' && $action == 'admin_index' ? 'active' : '' ?>"><a
                href="<?php echo $this->Html->url("/admin/users/index", true) ?>"><i
                    class="fa fa-users"></i><span>Total members</span></a>
        </li>
    <li class="<?php echo $ctrl == 'users' && $action == 'admin_settings' ? 'active' : '' ?>"><a
            href="<?php echo $this->Html->url("/admin/users/settings", true) ?>"><i
                class="fa fa-key"></i><span>Reset password</span></a>
    </li>
    

    <li class="treeview <?php echo $ctrl == 'helps' && $action == 'admin_add_file' || $ctrl == 'helps' && $action == 'admin_add_file' ? 'active menu-open' : '' ?>"><a
            href="#"><i
                class="fa fa-question"></i>
                <span>Help Text</span>
<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
        <ul class="treeview-menu">            
            <li class="<?php echo $ctrl == 'helps' && $action == 'admin_add_file' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/helps/add_file", true) ?>"><i
                        class="fa fa-upload"></i>Add</a></li>
                        <li class="<?php echo $ctrl == 'helps' && $action == 'admin_index' ? 'active' : '' ?>"><a
            href="<?php echo $this->Html->url("/admin/helps/index", true) ?>"><i
                class="fa fa-list"></i><span>List</span></a>
    </li>
        </ul>
    </li>

    <li class="treeview <?php echo $ctrl == 'tasbihoptions' && $action == 'admin_add_file' || $ctrl == 'tasbihoptions' && $action == 'admin_add_file' ? 'active menu-open' : '' ?>"><a
            href="#"><i
                class="fa fa-spinner"></i><span>Tasbih Tracker</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span></a>
        <ul class="treeview-menu">
            <li class="<?php echo $ctrl == 'tasbihoptions' && $action == 'admin_add_file' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/tasbihoptions/add_file", true) ?>"><i
                        class="fa fa-upload"></i> Add Tasbih Option</a></li>
            <li class="<?php echo $ctrl == 'tasbihoptions' && $action == 'admin_index' ? 'active' : '' ?>"><a
                    href="<?php echo $this->Html->url("/admin/tasbihoptions/index", true) ?>"><i
                        class="fa fa-list"></i><span>List</span></a>
        </ul>
    </li>

</ul>
</section>
<!-- /.sidebar -->
</aside>