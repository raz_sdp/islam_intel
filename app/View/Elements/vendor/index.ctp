<div class="box-body">

        <table id="example2" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <th><?php echo $this->Paginator->sort('company_name'); ?></th>
                <th><?php echo $this->Paginator->sort('fullname'); ?></th>
                <th><?php echo $this->Paginator->sort('email'); ?></th>
                <th><?php echo $this->Paginator->sort('address'); ?></th>
                <th><?php echo $this->Paginator->sort('city'); ?></th>
                <th><?php echo $this->Paginator->sort('mobile'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cc as $ccc): ?>
                <tr>
                    <td><?php echo h($ccc['User']['id']); ?>&nbsp;</td>
                    <td><?php echo h($ccc['User']['company_name']); ?>&nbsp;</td>
                    <td><?php echo h($ccc['User']['fullname']); ?>&nbsp;</td>
                    <td><?php echo h($ccc['User']['email']); ?>&nbsp;</td>
                    <td><?php echo h($ccc['User']['address']); ?>&nbsp;</td>
                    <td><?php echo h($ccc['User']['city']); ?>&nbsp;</td>
                    <td><?php echo h($ccc['User']['mobile']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ccc['User']['id'],'cc'),array('class' => 'btn btn-primary')); ?>
                        <?php echo $this->Html->link(__('Group'), array('controller' => 'vendorusers', 'action' => 'add', $ccc['User']['id']),array('class' => 'btn btn-default')); ?>
                        <?php echo $this->Html->link(__('Add test'), array('controller' => 'vendortests', 'action' => 'add', $ccc['User']['id']), array('class' => 'btn btn-success')); ?>
                        <?php echo $this->Html->link(__('List test'), array('controller' => 'vendortests', 'action' => 'index', $ccc['User']['id']), array('class' => 'btn btn-info')); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ccc['User']['id'],'cc'), array('confirm' => __('Are you sure you want to delete # %s?', $ccc['User']['id']), 'class' => 'btn btn-danger')); ?>
                        <?php echo $this->Html->link(__('Add questions'), array('controller' => 'vendorquestions', 'action' => 'add', $ccc['User']['id']), array('class' => 'btn btn-warning')); ?>
                        <?php echo $this->Html->link(__('List questions'), array('controller' => 'vendorquestions', 'action' => 'index', $ccc['User']['id']), array('class' => 'btn btn-primary')); ?>
                    </td>
                </tr>

            <?php endforeach; ?>

            </tbody>

        </table>