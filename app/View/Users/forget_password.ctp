<style>
    .logo-width {
        width: 50% !important;
        height: 50% !important;
        margin: auto;
    }
    .login-page, .register-page{
        background-color: #0a8ee3 !important;
    }
</style>
<div class="login-box">
    <div class="login-logo">
        <a href="/">
            <?php echo $this->Html->image('/img/logo.png', ['class' => 'img-responsive logo-width']) ?>
        </a>
        <!--<a href="/"><b>Admission</b>Ninja</a>-->
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <h3 class="login-box-msg">Reset Password</h3>

        <?php echo $this->Form->create('User', array('class' => 'form-group', 'data-toggle' => 'validator')); ?>
        <div class="form-group has-feedback">
            <!--<input type="password" class="form-control" placeholder="Password">-->
            <?php
            echo $this->Form->input('password', array('class' => 'form-control', 'required' => 'required','label' => false, 'placeholder' => 'Password', 'div' => array('class' => 'form-group')));
            ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <!--<input type="password" class="form-control" placeholder="Password">-->
            <?php
            echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'required' => 'required', 'data-match' => '#UserPassword','placeholder' => 'Confirm Password', 'div' => array('class' => 'form-group')));
            ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">

            <!-- /.col -->
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
            </div>
            <!-- /.col -->
        </div>
        <br>
        <!-- <p class="login-box-msg"><a href="#">I forgot my password</a></p> -->
        </form>


    </div>
    <!-- /.login-box-body -->

    <?php
echo $this->Html->script([
    'validator.min'

]);