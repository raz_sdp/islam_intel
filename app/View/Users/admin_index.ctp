<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">App Users</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
                    <div class="box-body">

                        <div class="row">

                            <!-- /.col -->
                        </div>

                        <!-- /.Search Box -->






                        <div class="box-body">
                            <div class='table-responsive'>
	<table id="example2" class="table table-bordered table-hover">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id','#'); ?></th>
			<th><?php echo $this->Paginator->sort('fullname'); ?></th>
			<th><?php echo $this->Paginator->sort('date of brith'); ?></th>
			<th><?php echo $this->Paginator->sort('gender'); ?></th>
			<th><?php echo $this->Paginator->sort('email'); ?></th>
            <th><?php echo $this->Paginator->sort('facebook'); ?></th>
            <th><?php echo $this->Paginator->sort('created','Date of Creation'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h(@$user['User']['fullname']); ?>&nbsp;</td>
		<td>
<!--            --><?php //echo h(@$user['User']['dob']); ?><!--&nbsp;-->
            <?php echo date_format(date_create($user['User']['dob']), 'd M Y'); ?>

        </td>
		<td><?php echo h(@$user['User']['gender']); ?>&nbsp;</td>
		<td><?php echo h(@$user['User']['email']); ?>&nbsp;</td>
        <td><?php echo h(@$user['User']['facebook']); ?>&nbsp;</td>
        <td><?php echo date_format(date_create($user['User']['created']), 'd M Y'); ?></td>
		<td class="actions">
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('class' =>'btn btn-danger','confirm' => __('Are you sure you want to delete %s?', $user['User']['fullname']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
                            </div>

                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
