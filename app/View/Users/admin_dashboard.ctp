<?php

/*App::uses('HttpSocket', 'Network/Http');
$HttpSocket = new HttpSocket();
//pr(AuthComponent::user());
$group_id = AuthComponent::user(['group_id']);
$results = $HttpSocket->get(Router::url("/subjects/get_lectures/".$group_id
    , true));
$tests = json_decode($results->body, true);*/

?>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">


    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box lectures">
                <div class="inner">
                    <h3><?php echo $users ?></h3>
                    <p>Total member</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/users/index", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box question_bank">
                <div class="inner">
                    <h3>Tasbih</h3>
                    <p>import options list</p>

                </div>
                <div class="icon">
                    <i class="fa fa-upload"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/tasbihoptions/add_file", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Reset</h3>
                    <p>Admin password</p>

                </div>
                <div class="icon">
                    <i class="fa fa-key"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/users/settings", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box model_test">
                <div class="inner">
                    <h3>Help</h3>
                    <p>import helps</p>

                </div>
                <div class="icon">
                    <i class="fa fa-upload"></i>
                </div>
                <a href="<?php echo $this->Html->url("/admin/helps/add_file", true) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>


        <!-- ./col -->

                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.box -->

</section>
<!-- right col -->
</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->