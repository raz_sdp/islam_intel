
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Change admin password</h3>
                    </div>

                    <div class="box-body">

                        <div class="box-body">
<form method="post" data-toggle="validator" role="form">
    <div class="form-group has-feedback">
        <label for="upload">Password</label>
        <input type="password" name="data[User][password]" class="form-control" placeholder="Password" id="upload" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
    </div>
    <div class="form-group has-feedback">
        <label for="upload-match">Re-type password</label>
        <input type="password" class="form-control" id="upload-match" data-match="#upload" data-match-error="password don't match" placeholder="Confirm" required>
        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
        <div class="help-block with-errors"></div>
    <div class="col-md-12">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <button class="btn btn-block btn-primary btn-lg" type="submit">Submit</button>
    </div>
        <div class="col-md-3"></div>
    </div>
</form>






                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>