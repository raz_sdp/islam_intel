<?php
/**
 * Created by PhpStorm.
 * User: mukul
 * Date: 8/1/18
 * Time: 1:17 PM
 */ ?>
    <style>
        .logo-width {
            width: 50% !important;
            height: 50% !important;
            margin: auto;
        }
        .login-page, .register-page{
            background-color: #0a8ee3 !important;
        }
        .displayhide {
            display: none;
        }
    </style>
<div class="login-box">
    <div class="login-logo">
        <a href="/">
            <?php echo $this->Html->image('/img/logo.png', ['class' => 'img-responsive logo-width']) ?>
        </a>
        <!--<a href="/"><b>Admission</b>Ninja</a>-->
    </div>
    <!-- /.login-logo -->
    <div class="alert alert-success displayhide" id="alertsuccess"></div>
    <div class="alert alert-danger displayhide" id="alertdanger"></div>
    <div class="login-box-body">Enter your Email</h3>

        <?php echo $this->Form->create('User', array('class' => 'form-group', 'data-toggle' => 'validator')); ?>
        <div class="form-group has-feedback">
            <!--<input type="password" class="form-control" placeholder="Password">-->
            <?php
            echo $this->Form->input('email', array('class' => 'form-control', 'required' => 'required','label' => false, 'placeholder' => 'Email', 'div' => array('class' => 'form-group')));
            ?>
            <span class="glyphicon form-control-feedback"></span>
        </div>
        <div class="row">

            <!-- /.col -->
            <div class="col-xs-12">
                <button type="button" class="btn btn-primary btn-block btn-flat" id="resetsubmit">Submit</button>
            </div>
            <!-- /.col -->
        </div>
        <br>
        <!-- <p class="login-box-msg"><a href="#">I forgot my password</a></p> -->
        </form>


    </div>
    <!-- /.login-box-body -->

<?php
echo $this->Html->script([
    'validator.min'

]);?>
    <script>
        $(function(){
            $('#resetsubmit').on('click', function(){
                console.log('click');
                var postData = {
                    'data[User][email]' : $('#UserEmail').val()
                };
//        console.log(postData)
                $.post(ROOT+'users/reset_password', postData, function(response){

                    if(response.success) {
                        console.log(response.msg);

                        $('#alertsuccess').show().text(response.msg)

                    } else {

                        $('#alertdanger').show().text(response.msg)

                    }

                },'json');
            });
        });
    </script>