<style>
    .logo-width{
        width: 50% !important;
        height: 50% !important;
        margin: auto;
    }
</style>
<div class="login-box">
  <div class="login-logo">
      <a href="/">
          <?php echo $this->Html->image('/img/logo.png',['class' => 'img-responsive logo-width'])?>
      </a>
    <!--<a href="/"><b>Admission</b>Ninja</a>-->
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

      <?php echo $this->Form->create('User',array('class'=>'form-group'));?>
      <div class="form-group has-feedback">
        <!--<input type="email" class="form-control" placeholder="Email">-->
        <?php
                echo $this->Form->input('email',array('class'=>'form-control','label'=>false,'placeholder'=>'Email','div'=>array('class'=>'form-group')));?>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <!--<input type="password" class="form-control" placeholder="Password">-->
        <?php
        echo $this->Form->input('password',array('class'=>'form-control','label'=>false,'placeholder'=>'Password', 'div'=>array('class'=>'form-group')));?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
          <br>
          <p class="text-center"><?php echo $this->Html->link(__('Forget Password?'), '/admin-forgot-password'); ?></p>
        <!-- /.col -->
      </div>
      <br>
      <!-- <p class="login-box-msg"><a href="#">I forgot my password</a></p> -->
    </form>
    

  </div>
  <!-- /.login-box-body -->
